Source: medialibrary
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders: Sebastian Ramacher <sramacher@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 libsqlite3-dev,
 libxxhash-dev,
 meson,
 ninja-build,
 pkgconf,
Standards-Version: 4.7.0
Section: libs
Homepage: https://code.videolan.org/videolan/medialibrary
Vcs-Browser: https://salsa.debian.org/multimedia-team/medialibrary
Vcs-Git: https://salsa.debian.org/multimedia-team/medialibrary.git
Rules-Requires-Root: no

Package: libmedialibrary-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libmedialibrary0 (= ${binary:Version}),
 libsqlite3-dev,
 libxxhash-dev,
 ${misc:Depends}
Description: library for managing media files in a media library (development files)
 Medialibrary provides tools for media applications to manage their media
 libraries. It supports media discovery, metadata handling, and a database
 backend. The latter provides ways to easily search and browse the media
 library.
 .
 This package contains the development files for medialibrary.

Package: libmedialibrary0
Architecture: any
Multi-Arch: same
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Description: library for managing media files in a media library (shared library)
 Medialibrary provides tools for media applications to manage their media
 libraries. It supports media discovery, metadata handling, and a database
 backend. The latter provides ways to easily search and browse the media
 library.
 .
 This package contains the shared library.
